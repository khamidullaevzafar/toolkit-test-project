import { Add, Remove } from "@mui/icons-material"
import { Button } from "@mui/material"
import { useEffect, useMemo } from "react"
import { useDispatch, useSelector } from "react-redux"
import RectangleIconButton from "../../components/Buttons/RectangleIconButton"
import { cartActions } from "../../store/cart/cart.slice"
import styles from "./style.module.scss"

const ProductItem = ({ product }) => {
  const dispatch = useDispatch()
  const cartProductsList = useSelector((state) => state.cart.list)

  const count = useMemo(() => {
    return cartProductsList.find((el) => el.id === product.id)?.count
  }, [cartProductsList, product])

  const addToCart = () => {
    dispatch(cartActions.addProduct(product))
  }

  const removeFromCart = () => {
    dispatch(cartActions.removeProduct(product))
  }

  useEffect(() => {
    console.log('FETCH =======')
  }, [])



  return (
    <div className={styles.item}>
      <img src={product.photo} className={styles.itemPhoto} alt="" />

      <p className={styles.itemName}>{product.name}</p>

      <p className={styles.price}>{product.price}сум</p>

      <div className={styles.cardFooter}>
        {count ? (
          <>
            <RectangleIconButton color="primary" onClick={removeFromCart} >
              <Remove />
            </RectangleIconButton>

            <p className={styles.count}>{count}</p>

            <RectangleIconButton color="primary" onClick={addToCart} >
              <Add />
            </RectangleIconButton>
          </>
        ) : (
          <Button
            onClick={addToCart}
            size="large"
            fullWidth
            variant="contained"
            color="primary"
          >
            Add to cart
          </Button>
        )}
      </div>
    </div>
  )
}

export default ProductItem

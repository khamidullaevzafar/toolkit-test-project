import { Button } from "@mui/material"
import { useState } from "react"
import { useSelector } from "react-redux"
import Header from "../../components/Header"
import ProductItem from "./ProductItem"
import styles from "./style.module.scss"

const ProductsPage = () => {
  const productList = useSelector((state) => state.products.list)
  const [counter, setCounter] = useState(0)

  return (
    <div>
      <Header
        title="Products"
        extra={
          <Button onClick={() => setCounter((prev) => prev + 1)}>
            Add {counter}
          </Button>
        }
      />

      <div className={styles.page}>
        {productList?.map((product) => (
          <ProductItem key={product.id} product={product} />
        ))}
      </div>
    </div>
  )
}

export default ProductsPage

import { Button } from "@mui/material"
import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import {
  CTable,
  CTableBody,
  CTableCell,
  CTableHead,
  CTableHeadRow,
  CTableRow,
} from "../../components/CTable"
import Header from "../../components/Header"
import { request } from "../../request"
import { cartActions } from "../../store/cart/cart.slice"
import CartItem from "./CartItem"
import styles from "./style.module.scss"





const CartPage = () => {
  const productList = useSelector(state => state.cart.list)
  const dispatch = useDispatch()


  const clearCart = () => {
    dispatch(cartActions.clearCart())
  }



  return (
    <div>
      <Header
        title="Cart"
        extra={
          <Button color="error" variant="contained" onClick={clearCart} >
            Clear cart
          </Button>
        }
      />
      <div className={styles.page}>
        <CTable>
          <CTableHead>
            <CTableHeadRow>
              <CTableCell> No </CTableCell>
              <CTableCell> Image </CTableCell>
              <CTableCell> Name </CTableCell>
              <CTableCell> Price </CTableCell>
              <CTableCell> Count </CTableCell>
              <CTableCell> Computed price </CTableCell>
              <CTableCell></CTableCell>
            </CTableHeadRow>
          </CTableHead>
          <CTableBody dataLength={10}>
            {
              productList?.map(product => (
                <CartItem key={product.id} product={product} />
              ))
            }
            
          </CTableBody>
        </CTable>
      </div>
    </div>
  )
}

export default CartPage

import { Delete } from "@mui/icons-material"
import { useDispatch } from "react-redux"
import RectangleIconButton from "../../components/Buttons/RectangleIconButton"
import { CTableCell, CTableRow } from "../../components/CTable"
import { cartActions } from "../../store/cart/cart.slice"
import styles from "./style.module.scss"

const CartItem = ({ product }) => {
  const dispatch = useDispatch()

  const deleteProduct = () => {
    dispatch(cartActions.deleteProduct(product))
  }

  


  return (
    <CTableRow className={styles.item}>
      <CTableCell>1</CTableCell>
      <CTableCell>
        <img
          src={product.photo}
          className={styles.itemPhoto}
          alt=""
        />
      </CTableCell>
      <CTableCell>{product.name}</CTableCell>
      <CTableCell>{product.price}</CTableCell>
      <CTableCell>{product.count}</CTableCell>
      <CTableCell>{product.count * product.price}</CTableCell>
      <CTableCell>
        <RectangleIconButton color="error" onClick={deleteProduct} >
          <Delete color="error" />
        </RectangleIconButton>
      </CTableCell>
    </CTableRow>
  )
}

export default CartItem

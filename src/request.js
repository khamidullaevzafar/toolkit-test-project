const products = [
  {
    id: 1,
    name: "Макси Бокс Популярный",
    price: 35000,
    photo:
      "https://cdn.delever.uz/delever/0dc1cf33-321e-450f-befe-63acc937d642",
  },
  {
    id: 2,
    name: "Макси бокс Ретро",
    price: 30000,
    photo:
      "https://cdn.delever.uz/delever/03c65f75-2051-4ac3-b0ff-5f2a1d148ce6",
  },
  {
    id: 3,
    name: "Макси Бокс Тренд",
    price: 30000,
    photo:
      "https://cdn.delever.uz/delever/5ffe3b76-58cd-4a0f-bc9b-cbad1276cd88",
  },
  {
    id: 4,
    name: "Макси Бокс Традиция",
    price: 30000,
    photo:
      "https://cdn.delever.uz/delever/5dc67c05-5544-4348-a478-7250cd2b6fe1",
  },
  {
    id: 5,
    name: "Лаваш Куриный Standart Classic",
    price: 20000,
    photo:
      "https://cdn.delever.uz/delever/4db5ad83-b34e-48e8-ad57-b27e7fe96979",
  },
  {
    id: 6,
    name: "Лаваш Standart classic",
    price: 22000,
    photo:
      "https://cdn.delever.uz/delever/463f704a-a4df-4696-b7f6-29f1f35c5eb8",
  },
  {
    id: 7,
    name: "Лаваш мясной Standart острый",
    price: 20000,
    photo:
      "https://cdn.delever.uz/delever/d4021fb8-f1d1-40e7-8059-d9a77370d565",
  },
  {
    id: 8,
    name: "Лаваш Куриный Standart Cheese",
    price: 22000,
    photo:
      "https://cdn.delever.uz/delever/88bfe210-4aec-44cc-a85b-4ab4e18f7c9a",
  },
]

export const request = {
  get(url) {
    switch (url) {
      case "/products":
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve({ products })
          }, 1000)
        })

      default:
        return null
    }
  },

  post(url) {
    switch (url) {
      case "/login":
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            reject(
              new Error({ errors: { email: "Неверный логин или пароль" } })
            )
          }, 1000)
        })

      default:
        return null
    }
  },
}

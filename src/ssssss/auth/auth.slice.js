import { createSlice } from "@reduxjs/toolkit"
import { loginAction } from "./auth.thunk"


export const {
  actions: authActions,
  reducer: authReducer
} = createSlice({
  name: "auth",
  initialState: {
    isAuth: true,
    userInfo: {},
    errors: null,
  },
  reducers: {
    login: (state) => {
      state.isAuth = true
    },
    logout: (state) => {
      state.isAuth = false
    }
  },
  extraReducers: {
    [loginAction.fulfilled]: (state, action) => {
      state.userInfo = action.payload
      state.isAuth = true
    },
  }
})
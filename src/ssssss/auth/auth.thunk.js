import { createAsyncThunk } from "@reduxjs/toolkit";
import { request } from "../../request";


export const loginAction = createAsyncThunk(
  'auth/login',
  async (data, { dispatch }) => {
      const res = await request.post('/login', data)

      return res.user
  }
)
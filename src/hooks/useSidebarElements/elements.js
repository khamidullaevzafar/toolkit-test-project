import { Widgets, ShoppingCart } from "@mui/icons-material"

export const elements = [
  {
    id: "products",
    title: "Products",
    path: "/products",
    icon: Widgets,
  },
  {
    id: "cart",
    title: "Cart",
    path: "/cart",
    icon: ShoppingCart,
  },
]

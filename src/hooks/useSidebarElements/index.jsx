import { elements } from "./elements"

const useSidebarElements = () => {

  return { elements: elements ?? [] }
}

export default useSidebarElements

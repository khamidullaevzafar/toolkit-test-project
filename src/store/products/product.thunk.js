import { createAsyncThunk } from "@reduxjs/toolkit";
import { request } from "../../request";




export const getProductsListAction = createAsyncThunk(
  'products/getProductsList',
  async (payload, { dispatch, getState }) => {
    const res = await request.get('/products')
    return res.products
  }
)
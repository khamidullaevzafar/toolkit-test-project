
import { createSlice } from '@reduxjs/toolkit'
import { getProductsListAction } from './product.thunk'




const productSlice = createSlice({
  initialState: {
    list: [],
  },
  name: "products",
  reducers: {
    setList(state, action) {
      state.list = action.payload
    }
  },
  extraReducers: {
    [getProductsListAction.fulfilled]: (state, action) => {
      state.list = action.payload
    }
  }
})

export const { reducer: productReducer, actions: productActions } = productSlice
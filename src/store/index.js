import { configureStore } from "@reduxjs/toolkit";
import { cartReducer } from "./cart/cart.slice";
import { productReducer } from "./products/product.slice";




export const store = configureStore({
  reducer: {
    products: productReducer,
    cart: cartReducer
  }
})




import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  list: [],
}

export const { reducer: cartReducer, actions: cartActions } = createSlice({
  initialState,
  name: "cart",
  reducers: {
    addProduct(state, action) {
      const index = state.list.findIndex(item => item.id === action.payload.id)

      if (index === -1) {
        state.list.push({
          ...action.payload,
          count: 1
        })
      } else {
        state.list[index].count++
      }      
    },
    removeProduct(state, action) {
      const index = state.list.findIndex(item => item.id === action.payload.id)

      if(state.list[index].count > 1) {
        state.list[index].count--
      } else {
        state.list.splice(index, 1)
      }

    },
    deleteProduct(state, action) {
      const index = state.list.findIndex(item => item.id === action.payload.id)
      state.list.splice(index, 1)
    },
    clearCart(state) {
      return initialState
    }
  },  
})

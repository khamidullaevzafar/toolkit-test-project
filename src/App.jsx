import { Provider, useDispatch } from "react-redux"
import { BrowserRouter } from "react-router-dom"
import { PersistGate } from "redux-persist/integration/react"
import GlobalFunctionsProvider from "./providers/GlobalFunctionsProvider"
import MaterialUIProvider from "./providers/MaterialUIProvider"
import Router from "./router"
import "./i18next"
import { Suspense, useEffect } from "react"
import PageFallback from "./components/PageFallback"
import { store } from "./store"
import { getProductsListAction } from "./store/products/product.thunk"

function App() {


  return (
    <Suspense fallback={<PageFallback />} >
      <div className="App">
        {/* <Provider store={store}>
          <PersistGate persistor={persistor}> */}
            
            <Provider store={store} >

            <MaterialUIProvider>
                <BrowserRouter>
                  <Router />
                </BrowserRouter>
            </MaterialUIProvider>

            </Provider>


          {/* </PersistGate>
        </Provider> */}
      </div>
    </Suspense>
  )
}

export default App

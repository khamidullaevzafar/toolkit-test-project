import { useEffect } from "react"
import { useDispatch } from "react-redux"
import { Navigate, Route, Routes } from "react-router-dom"
import AuthLayout from "../layouts/AuthLayout"
import MainLayout from "../layouts/MainLayout"
import { getProductsListAction } from "../store/products/product.thunk"
import Login from "../views/Auth/Login"
import Registration from "../views/Auth/Registration"
import CartPage from "../views/Cart"
import ProductsPage from "../views/Products"

const Router = () => {
  // const isAuth = useSelector((state) => state.auth.isAuth)

  // if (!isAuth)
  //   return (
  //     <Routes>
  //       <Route path="/" element={<AuthLayout />}>
  //         <Route index element={<Navigate to="/login " />} />
  //         <Route path="login" element={<Login />} />
  //         <Route path="registration" element={<Registration />} />
  //         <Route path="*" element={<Navigate to="/login" />} />
  //       </Route>
  //       <Route path="*" element={<Navigate to="/login" />} />
  //     </Routes>
  //   )


  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getProductsListAction())
  }, [])
  

  return (
    <Routes>
      <Route path="/" element={<MainLayout />}>
        <Route index element={<Navigate to="/products" />} />

        <Route path="products" element={<ProductsPage />} />
        <Route path="cart" element={<CartPage />} />


        <Route path="*" element={<Navigate to="products" />} />
      </Route>
      <Route path="*" element={<Navigate to="products" />} />
    </Routes>
  )
}

export default Router
